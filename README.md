# KDE Akademy Workshop 2020

- [Event](https://conf.kde.org/en/akademy2020/public/schedule/1)
- [Slides](https://docs.google.com/presentation/d/11U_ZIuqoUfPUV5ctc9K6QCHvID7xSdpJpoU6ZgwdnqQ/edit?usp=sharing)

Repositories:

- [Primary](https://gitlab.com/dnsmichi/kde-akademy-workshop-2020)
- [Mirror](https://invent.kde.org/dnsmichi/kde-akademy-workshop-2020)

## Solutions

- `main.go`, `version.go` and `version_test.go` contain commented code used for CI/CD and test coverage exercises.

## Troubleshooting

- `.gitlab-ci.yml` needs an updated `REPO_NAME` for building Go with CI/CD
- `go.mod` might need a repository name update 

## Development

Initialize Go modules once.

```
go mod init
```

Download new dependencies during development.

```
go mod tidy
```
